var webpack = require('webpack');

var plugins = [
  new webpack.DefinePlugin({
  	'process.env': { NODE_ENV: '"production"'}
  })
];

module.exports = {
  mode: 'production',
  entry: ['./DateTime.js'],

  devtool: '#cheap-module-source-map',

  output: {
    path: __dirname + '/dist/',
    library: 'Datetime',
    libraryTarget: 'umd'
  },

  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM',
    'moment': 'moment'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ],
  },

  plugins: plugins
};
