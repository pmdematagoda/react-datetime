var gulp = require('gulp'),
	insert = require('gulp-insert'),
	plumber = require('gulp-plumber'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
	through = require('through2'),
	uglify = require('gulp-uglify'),
	webpack = require('webpack-stream'),
	webpackConfig = require('./webpack.config')
	;

var pack = require( './package.json' );

const subTask = (cb) => {
	// Reason behind having sub as separate task:
	// https://github.com/shama/webpack-stream/issues/114
	return gulp.src( './DateTime.js' )
		.pipe( webpack( getWebpackConfig() ) )
		.pipe( gulp.dest( 'tmp/' ) );
};

const buildTask = () => {
	return gulp.src( ['tmp/react-datetime.js'] )
		.pipe( sourcemaps.init( { loadMaps: true } ) )
			.pipe( through.obj( function( file, enc, cb ) {
				// Dont pipe through any source map files as
				// it will be handled by gulp-sourcemaps
				const isSourceMap = /\.map$/.test( file.path );
				if ( !isSourceMap ) this.push( file );
				cb();
			}))
			.pipe( plumber() )
			// .pipe( babel( { presets: [ 'es2015'] } ) )
			.pipe( insert.prepend( setHeader ) )
			.pipe( gulp.dest( 'dist/' ) ) // Save .js
			.pipe( uglify() )
			.pipe( insert.prepend( setHeader ) )
			.pipe( rename( { extname: '.min.js' } ) )
		.pipe( sourcemaps.write( '.' ) )
		.pipe( gulp.dest( 'dist/' ) ); // Save .min.js
	// TODO: Remove tmp folder
};

exports.build = gulp.series(subTask, buildTask);
exports.default = gulp.series(subTask, buildTask);

/*
 * Utility functions
 */

const getWebpackConfig = () => {
	return Object.assign(webpackConfig, {
		output: {
			filename: 'react-datetime.js',
			path: undefined,
		}
	});
};

const setHeader = ( '/*\n%%name%% v%%version%%\n%%homepage%%\n%%license%%: https://gitlab.com/pmdematagoda/react-datetime/raw/master/LICENSE\n*/\n' )
		.replace( '%%name%%', pack.name)
		.replace( '%%version%%', pack.version)
		.replace( '%%license%%', pack.license)
		.replace( '%%homepage%%', pack.homepage)
	;
