import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';

const createDays = (days) => {
    return days.map((day, index) => {
        return <th key={day + index} className="dow">
            {day}
        </th>;
    });
};

class DateTimePickerDays extends React.Component {
    render () {
        var footer = this.renderFooter(),
            date = this.props.viewDate,
            locale = date.localeData(),
            tableChildren
			;

        tableChildren = [
            <thead key="th">
                <tr key="h">
                    <th key="p" className="rdtPrev" onClick={this.props.subtractTime( 1, 'months' )}>
                        <span>‹</span>
                    </th>
                    <th key="s" colSpan={5} className="rdtSwitch" onClick={this.props.showView( 'months' )} data-value={this.props.viewDate.month()}>
                        {locale.months( date ) + ' ' + date.year()}
                    </th>
                    <th key="n" className="rdtNext" onClick={this.props.addTime( 1, 'months' )}>
                        <span>›</span>
                    </th>
                </tr>
                <tr key="d">
                    {createDays(this.getDaysOfWeek(locale))}
                </tr>
            </thead>,
            <tbody key="tb">
                {this.renderDays()}
            </tbody>
        ];

        if ( footer )
            tableChildren.push( footer );

        return <div className="rdtDays">
            <table>
                {tableChildren}
            </table>
        </div>;
    }

    /**
	 * Get a list of the days of the week
	 * depending on the current locale
	 * @return {array} A list with the shortname of the days
	 */
    getDaysOfWeek ( locale ) {
        var days = locale._weekdaysMin,
            first = locale.firstDayOfWeek(),
            dow = [],
            i = 0
			;

        days.forEach( function( day ) {
            dow[ (7 + ( i++ ) - first) % 7 ] = day;
        });

        return dow;
    }

    renderDays () {
        var date = this.props.viewDate,
            selected = this.props.selectedDate && this.props.selectedDate.clone(),
            prevMonth = date.clone().subtract( 1, 'months' ),
            currentYear = date.year(),
            currentMonth = date.month(),
            weeks = [],
            days = [],
            renderer = this.props.renderDay || this.renderDay,
            isValid = this.props.isValidDate || this.alwaysValidDate,
            classes, isDisabled, dayProps, currentDate
			;

        // Go to the last week of the previous month
        prevMonth.date( prevMonth.daysInMonth() ).startOf( 'week' );
        var lastDay = prevMonth.clone().add( 42, 'd' );

        while ( prevMonth.isBefore( lastDay ) ) {
            classes = 'rdtDay';
            currentDate = prevMonth.clone();

            if ( ( prevMonth.year() === currentYear && prevMonth.month() < currentMonth ) || ( prevMonth.year() < currentYear ) )
                classes += ' rdtOld';
            else if ( ( prevMonth.year() === currentYear && prevMonth.month() > currentMonth ) || ( prevMonth.year() > currentYear ) )
                classes += ' rdtNew';

            if ( selected && prevMonth.isSame( selected, 'day' ) )
                classes += ' rdtActive';

            if ( prevMonth.isSame( moment(), 'day' ) )
                classes += ' rdtToday';

            isDisabled = !isValid( currentDate, selected );
            if ( isDisabled )
                classes += ' rdtDisabled';

            dayProps = {
                key: prevMonth.format( 'M_D' ),
                'data-value': prevMonth.date(),
                className: classes
            };

            if ( !isDisabled )
                dayProps.onClick = this.updateSelectedDate.bind(this);

            days.push( renderer( dayProps, currentDate, selected ) );

            if ( days.length === 7 ) {
                weeks.push( React.createElement('tr', { key: prevMonth.format( 'M_D' )}, days ) );
                days = [];
            }

            prevMonth.add( 1, 'd' );
        }

        return weeks;
    }

    updateSelectedDate ( event ) {
        this.props.updateSelectedDate( event, true );
    }

    renderDay ( props, currentDate ) {
        return React.createElement('td',  props, currentDate.date() );
    }

    renderFooter () {
        if ( !this.props.timeFormat )
            return '';

        var date = this.props.selectedDate || this.props.viewDate;

        return <tfoot key="tf">
            <tr>
                <td onClick={this.props.showView( 'time' )} colSpan={7} className="rdtTimeToggle">{date.format(this.props.timeFormat)}</td>
            </tr>
        </tfoot>;
    }

    alwaysValidDate () {
        return 1;
    }

    handleClickOutside () {
        this.props.handleClickOutside();
    }
}

DateTimePickerDays.propTypes = {
    handleClickOutside: PropTypes.func,
    timeFormat: PropTypes.string,
    updateSelectedDate: PropTypes.func,
    isValidDate: PropTypes.func,
    selectedDate: PropTypes.object,
    renderDay: PropTypes.func,
    addTime: PropTypes.func,
    subtractTime: PropTypes.func,
    viewDate: PropTypes.object,
    showView: PropTypes.func,
};

export default onClickOutside(DateTimePickerDays);
