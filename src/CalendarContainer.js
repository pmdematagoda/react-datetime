import React from 'react';

import DaysView from './DaysView';
import MonthsView from './MonthsView';
import YearsView from './YearsView';
import TimeView from './TimeView';

const viewComponents = {
    days: DaysView,
    months: MonthsView,
    years: YearsView,
    time: TimeView
};

class CalendarContainer extends React.Component {
    render() {
        const ViewComponent = viewComponents[this.props.view];

        return <ViewComponent {...this.props.viewProps} />;
    }
}

export default CalendarContainer;
