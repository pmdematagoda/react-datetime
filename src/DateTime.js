import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import CalendarContainer from './CalendarContainer';

var viewModes = Object.freeze({
    YEARS: 'years',
    MONTHS: 'months',
    DAYS: 'days',
    TIME: 'time',
});

const allowedSetTime = ['hours', 'minutes', 'seconds', 'milliseconds'];

const componentProps = Object.freeze({
    fromProps: ['value', 'isValidDate', 'renderDay', 'renderMonth', 'renderYear', 'timeConstraints'],
    fromState: ['viewDate', 'selectedDate', 'updateOn'],
    fromThis: ['setDate', 'setTime', 'showView', 'addTime', 'subtractTime', 'updateSelectedDate', 'localMoment', 'handleClickOutside']
});

const getFormats = props => {
    var formats = {
            date: props.dateFormat || '',
            time: props.timeFormat || ''
        },
        locale = getLocalMoment( props.date, null, props ).localeData()
		;

    if ( formats.date === true ) {
        formats.date = locale.longDateFormat('L');
    }
    else if ( getUpdateOn(formats) !== viewModes.DAYS ) {
        formats.time = '';
    }

    if ( formats.time === true ) {
        formats.time = locale.longDateFormat('LT');
    }

    formats.datetime = formats.date && formats.time ?
        formats.date + ' ' + formats.time :
        formats.date || formats.time
    ;

    return formats;
};

const getLocalMoment = (date, format, props) => {
    var momentFn = props.utc ? moment.utc : moment;
    var m = momentFn( date, format, props.strictParsing );
    if ( props.locale )
        m.locale( props.locale );
    return m;
};

const getUpdateOn = formats => {
    if ( formats.date.match(/[lLD]/) ) {
        return viewModes.DAYS;
    } else if ( formats.date.indexOf('M') !== -1 ) {
        return viewModes.MONTHS;
    } else if ( formats.date.indexOf('Y') !== -1 ) {
        return viewModes.YEARS;
    }

    return viewModes.DAYS;
};

const parseDate = (date, formats, props) => {
    var parsedDate;

    if (date && typeof date === 'string')
        parsedDate = getLocalMoment(date, formats.datetime, props);
    else if (date)
        parsedDate = getLocalMoment(date, null, props);

    if (parsedDate && !parsedDate.isValid())
        parsedDate = null;

    return parsedDate;
};

const getStateFromProps = props => {
    var formats = getFormats( props ),
        date = props.value || props.defaultValue,
        selectedDate, viewDate, updateOn, inputValue
		;

    selectedDate = parseDate(date, formats, props);

    viewDate = parseDate(props.viewDate, formats, props);

    viewDate = selectedDate ?
        selectedDate.clone().startOf('month') :
        viewDate ? viewDate.clone().startOf('month') : getLocalMoment(undefined, undefined, props).startOf('month');

    updateOn = getUpdateOn(formats);

    if ( selectedDate )
        inputValue = selectedDate.format(formats.datetime);
    else if ( date.isValid && !date.isValid() )
        inputValue = '';
    else
        inputValue = date || '';

    return {
        updateOn: updateOn,
        inputFormat: formats.datetime,
        viewDate: viewDate,
        selectedDate: selectedDate,
        inputValue: inputValue,
        open: props.open
    };
};

var TYPES = PropTypes;
class Datetime extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.getInitialState();

        this.openCalendar = this.openCalendar.bind(this);
        this.closeCalendar = this.closeCalendar.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onInputKey = this.onInputKey.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.updateSelectedDate = this.updateSelectedDate.bind(this);

        this.subtractTime = this.subtractTime.bind(this);
        this.addTime = this.addTime.bind(this);
        this.setDate = this.setDate.bind(this);
        this.showView = this.showView.bind(this);
        this.setTime = this.setTime.bind(this);
    }

    getInitialState() {
        var state = getStateFromProps( this.props );

        if ( state.open === undefined )
            state.open = !this.props.input;

        state.currentView = this.props.dateFormat ?
            (this.props.viewMode || state.updateOn || viewModes.DAYS) : viewModes.TIME;

        return state;
    }

    // eslint-disable-next-line camelcase
    UNSAFE_componentWillReceiveProps( nextProps ) {
        var formats = getFormats( nextProps ),
            updatedState = {}
		;

        if ( nextProps.value !== this.props.value ||
			formats.datetime !== getFormats( this.props ).datetime ) {
            updatedState = getStateFromProps( nextProps );
        }

        if ( updatedState.open === undefined ) {
            if ( typeof nextProps.open !== 'undefined' ) {
                updatedState.open = nextProps.open;
            } else if ( this.props.closeOnSelect && this.state.currentView !== viewModes.TIME ) {
                updatedState.open = false;
            } else {
                updatedState.open = this.state.open;
            }
        }

        if ( nextProps.viewMode !== this.props.viewMode ) {
            updatedState.currentView = nextProps.viewMode;
        }

        if ( nextProps.locale !== this.props.locale ) {
            if ( this.state.viewDate ) {
                var updatedViewDate = this.state.viewDate.clone().locale( nextProps.locale );
                updatedState.viewDate = updatedViewDate;
            }
            if ( this.state.selectedDate ) {
                var updatedSelectedDate = this.state.selectedDate.clone().locale( nextProps.locale );
                updatedState.selectedDate = updatedSelectedDate;
                updatedState.inputValue = updatedSelectedDate.format( formats.datetime );
            }
        }

        if ( nextProps.utc !== this.props.utc ) {
            if ( nextProps.utc ) {
                if ( this.state.viewDate )
                    updatedState.viewDate = this.state.viewDate.clone().utc();
                if ( this.state.selectedDate ) {
                    updatedState.selectedDate = this.state.selectedDate.clone().utc();
                    updatedState.inputValue = updatedState.selectedDate.format( formats.datetime );
                }
            } else {
                if ( this.state.viewDate )
                    updatedState.viewDate = this.state.viewDate.clone().local();
                if ( this.state.selectedDate ) {
                    updatedState.selectedDate = this.state.selectedDate.clone().local();
                    updatedState.inputValue = updatedState.selectedDate.format(formats.datetime);
                }
            }
        }

        if ( nextProps.viewDate !== this.props.viewDate ) {
            updatedState.viewDate = moment(nextProps.viewDate);
        }
        //we should only show a valid date if we are provided a isValidDate function. Removed in 2.10.3
        /*if (this.props.isValidDate) {
			updatedState.viewDate = updatedState.viewDate || this.state.viewDate;
			while (!this.props.isValidDate(updatedState.viewDate)) {
				updatedState.viewDate = updatedState.viewDate.add(1, 'day');
			}
		}*/
        this.setState( updatedState );
    }

    onInputChange( e ) {
        var value = e.target === null ? e : e.target.value,
            localMoment = getLocalMoment( value, this.state.inputFormat, this.props ),
            update = { inputValue: value }
			;

        if ( localMoment.isValid() && !this.props.value ) {
            update.selectedDate = localMoment;
            update.viewDate = localMoment.clone().startOf('month');
        } else {
            update.selectedDate = null;
        }

        return this.setState( update, function() {
            return this.props.onChange( localMoment.isValid() ? localMoment : this.state.inputValue );
        });
    }

    onInputKey( e ) {
        if ( e.which === 9 && this.props.closeOnTab ) {
            this.closeCalendar();
        }
    }

    showView( view ) {
        var me = this;
        return function() {
            me.state.currentView !== view && me.props.onViewModeChange( view );
            me.setState({ currentView: view });
        };
    }

    setDate( type ) {
        var me = this,
            nextViews = {
                month: viewModes.DAYS,
                year: viewModes.MONTHS,
            }
		;
        return function( e ) {
            me.setState({
                viewDate: me.state.viewDate.clone()[ type ]( parseInt(e.target.getAttribute('data-value'), 10) ).startOf( type ),
                currentView: nextViews[ type ]
            });
            me.props.onViewModeChange( nextViews[ type ] );
        };
    }

    subtractTime( amount, type, toSelected ) {
        var me = this;
        return function() {
            me.props.onNavigateBack( amount, type );
            me.updateTime( 'subtract', amount, type, toSelected );
        };
    }

    addTime( amount, type, toSelected ) {
        var me = this;
        return function() {
            me.props.onNavigateForward( amount, type );
            me.updateTime( 'add', amount, type, toSelected );
        };
    }

    updateTime( op, amount, type, toSelected ) {
        var update = {},
            date = toSelected ? 'selectedDate' : 'viewDate';

        update[ date ] = this.state[ date ].clone()[ op ]( amount, type );

        this.setState( update );
    }

    setTime( type, value ) {
        var index = allowedSetTime.indexOf( type ) + 1,
            state = this.state,
            date = (state.selectedDate || state.viewDate).clone(),
            nextType
            ;

        // It is needed to set all the time properties
        // to not to reset the time
        date[ type ]( value );
        for (; index < allowedSetTime.length; index++) {
            nextType = allowedSetTime[index];
            date[ nextType ]( date[nextType]() );
        }

        if ( !this.props.value ) {
            this.setState({
                selectedDate: date,
                inputValue: date.format( state.inputFormat )
            });
        }
        this.props.onChange( date );
    }

    updateSelectedDate ( e, close ) {
        var target = e.target,
            modifier = 0,
            viewDate = this.state.viewDate,
            currentDate = this.state.selectedDate || viewDate,
            date
			;

        if (target.className.indexOf('rdtDay') !== -1) {
            if (target.className.indexOf('rdtNew') !== -1)
                modifier = 1;
            else if (target.className.indexOf('rdtOld') !== -1)
                modifier = -1;

            date = viewDate.clone()
                .month( viewDate.month() + modifier )
                .date( parseInt( target.getAttribute('data-value'), 10 ) );
        } else if (target.className.indexOf('rdtMonth') !== -1) {
            date = viewDate.clone()
                .month( parseInt( target.getAttribute('data-value'), 10 ) )
                .date( currentDate.date() );
        } else if (target.className.indexOf('rdtYear') !== -1) {
            date = viewDate.clone()
                .month( currentDate.month() )
                .date( currentDate.date() )
                .year( parseInt( target.getAttribute('data-value'), 10 ) );
        }

        date.hours( currentDate.hours() )
            .minutes( currentDate.minutes() )
            .seconds( currentDate.seconds() )
            .milliseconds( currentDate.milliseconds() );

        if ( !this.props.value ) {
            var open = !( this.props.closeOnSelect && close );
            if ( !open ) {
                this.props.onBlur( date );
            }

            this.setState({
                selectedDate: date,
                viewDate: date.clone().startOf('month'),
                inputValue: date.format( this.state.inputFormat ),
                open: open
            });
        } else {
            if ( this.props.closeOnSelect && close ) {
                this.closeCalendar();
            }
        }

        this.props.onChange( date );
    }

    openCalendar( e ) {
        if ( !this.state.open ) {
            this.setState({ open: true }, function() {
                this.props.onFocus( e );
            });
        }
    }

    closeCalendar() {
        this.setState({ open: false }, function () {
            this.props.onBlur( this.state.selectedDate || this.state.inputValue );
        });
    }

    handleClickOutside() {
        if ( this.props.input && this.state.open && !this.props.open && !this.props.disableOnClickOutside ) {
            this.setState({ open: false }, function() {
                this.props.onBlur( this.state.selectedDate || this.state.inputValue );
            });
        }
    }

    getComponentProps() {
        var me = this,
            formats = getFormats( this.props ),
            props = {dateFormat: formats.date, timeFormat: formats.time}
			;

        componentProps.fromProps.forEach( function( name ) {
            props[ name ] = me.props[ name ];
        });
        componentProps.fromState.forEach( function( name ) {
            props[ name ] = me.state[ name ];
        });
        componentProps.fromThis.forEach( function( name ) {
            props[ name ] = me[ name ];
        });

        return props;
    }

    render () {
        // TODO: Make a function or clean up this code,
        // logic right now is really hard to follow
        var className = 'rdt' + (this.props.className ?
                ( Array.isArray( this.props.className ) ?
                    ' ' + this.props.className.join( ' ' ) : ' ' + this.props.className) : ''),
            children = [];

        if ( this.props.input ) {
            var finalInputProps = Object.assign({
                type: 'text',
                className: 'form-control',
                onClick: this.openCalendar,
                onFocus: this.openCalendar,
                onChange: this.onInputChange,
                onKeyDown: this.onInputKey,
                value: this.state.inputValue,
            }, this.props.inputProps);
            if ( this.props.renderInput ) {
                children = [ React.createElement('div', { key: 'i' }, this.props.renderInput( finalInputProps, this.openCalendar, this.closeCalendar )) ];
            } else {
                children = [ React.createElement('input', Object.assign({ key: 'i' }, finalInputProps ))];
            }
        } else {
            className += ' rdtStatic';
        }

        if ( this.state.open )
            className += ' rdtOpen';

        return <div className={className}>
            {children}
            <div key="dt" className="rdtPicker">
                <CalendarContainer view={this.state.currentView} viewProps={this.getComponentProps()} onClickOutside={this.handleClickOutside} />
            </div>
        </div>;
    }
}

Datetime.propTypes = {
    value: TYPES.oneOfType([TYPES.object, TYPES.string]),
    defaultValue: TYPES.oneOfType([TYPES.object, TYPES.string, TYPES.number, PropTypes.instanceOf(Date)]),
    viewDate: TYPES.oneOfType([TYPES.object, TYPES.string]),
    onFocus: TYPES.func,
    onBlur: TYPES.func,
    onChange: TYPES.func,
    onViewModeChange: TYPES.func,
    onNavigateBack: TYPES.func,
    onNavigateForward: TYPES.func,
    locale: TYPES.string,
    utc: TYPES.bool,
    input: TYPES.bool,
    dateFormat: TYPES.oneOfType([TYPES.string, TYPES.bool]),
    timeFormat: TYPES.oneOfType([TYPES.string, TYPES.bool]),
    inputProps: TYPES.object,
    timeConstraints: TYPES.object,
    viewMode: TYPES.oneOf([viewModes.YEARS, viewModes.MONTHS, viewModes.DAYS, viewModes.TIME]),
    isValidDate: TYPES.func,
    open: TYPES.bool,
    strictParsing: TYPES.bool,
    closeOnSelect: TYPES.bool,
    closeOnTab: TYPES.bool,
    className: TYPES.oneOfType([TYPES.string, TYPES.array]),
    disableOnClickOutside: TYPES.bool,
    renderInput: TYPES.func,
};

Datetime.defaultProps = {
    className: '',
    defaultValue: '',
    inputProps: {},
    input: true,
    onFocus: function() {},
    onBlur: function() {},
    onChange: function() {},
    onViewModeChange: function() {},
    onNavigateBack: function() {},
    onNavigateForward: function() {},
    timeFormat: true,
    timeConstraints: {},
    dateFormat: true,
    strictParsing: true,
    closeOnSelect: false,
    closeOnTab: true,
    utc: false
};

// Make moment accessible through the Datetime class
Datetime.moment = moment;

export default Datetime;
