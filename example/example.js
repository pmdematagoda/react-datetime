import DateTime from '../src/DateTime.js';

var React = require('react');
var ReactDOM = require('react-dom');

ReactDOM.render(
  <DateTime
    viewMode='months'
    dateFormat='MMMM'
    isValidDate={current => current.isBefore(DateTime.moment().startOf('month'))}
  />,
  document.getElementsByClassName('datetime-months-validated')[0]
);

ReactDOM.render(
  <DateTime
    viewMode='years'
    dateFormat='YYYY'
  />,
  document.getElementsByClassName('datetime-years')[0]
);

ReactDOM.render(
  <DateTime
    viewMode='days'
    dateFormat='ddd-MM-YYYY'
  />,
  document.getElementsByClassName('datetime-days')[0]
);

ReactDOM.render(
  <DateTime
    viewMode='time'
    dateFormat='MMMM Do YYYY,'
    timeFormat='HH:mm:ss a'
  />,
  document.getElementsByClassName('datetime-time')[0]
);
